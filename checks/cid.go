package checks

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

// Getcid collects client id from file
func Getcid() string {
	// Read file into memory
	data, err := ioutil.ReadFile("/tmp/test.txt")
	// Check for errors
	check(err)
	fmt.Println(string(data))

	// Parse contents of file and use "FieldsFunc" to seperate sections by ':'
	sds := strings.FieldsFunc(string(data), func(r rune) bool {
		if r == ':' {
			return true
		}
		return false
	})
	// Grab client ID out of newly created slice of string
	cid := sds[0]

	return cid
}
