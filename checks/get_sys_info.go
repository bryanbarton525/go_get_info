package checks

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"sync"

	"github.com/shirou/gopsutil/cpu"
	"github.com/shirou/gopsutil/mem"
	"gitlab.com/bryanbarton525/go_get_info/getconfigs"
)

// Jblob is a new struct to translate imported Infostat struct to a usable block
type Jblob struct {
	Model             string  `json:"model"`
	Cores             int     `json:"cores"`
	Processors        int32   `json:"processors"`
	CPUSpeed          float64 `json:"cpu_speed"`
	TotalMem          uint64  `json:"total"`
	UsedMem           uint64  `json:"used"`
	FreeMem           uint64  `json:"available"`
	ProductName       string  `json:"product_name"`
	ProductModel      string  `json:"product_model"`
	ProductServiceTag string  `json:"service_tag"`
}

var wg sync.WaitGroup
var configs = getconfigs.GetConfigs()

func modeSelect() map[string]string {
	var paths map[string]string
	if configs.Mode == "DEV" {
		paths = configs.Dev
	} else if configs.Mode == "PROD" {
		paths = configs.Prod
	} else {
		panic("No mode defined")
	}
	return paths
}

// GetSysInfo returns all
func GetSysInfo() Jblob {
	var jdata Jblob
	wg.Add(3)
	go jdata.collectMemInfo()
	go jdata.collectProcInfo()
	go jdata.collectProductInfo()
	wg.Wait()
	return jdata
}

// CollectProcInfo collects info about the processor
func (j *Jblob) collectProcInfo() {

	fulldump, err := cpu.Info()
	if err != nil {
		fmt.Println(err)
		wg.Done()
	}
	cores := len(fulldump) - 1
	// slice out last core info
	procdata := fulldump[cores]
	fmt.Println("procdata:", procdata)
	fmt.Println("CoreID:", procdata.CoreID)
	fmt.Printf("%T\n", procdata.CoreID)

	procs, err := strconv.Atoi(procdata.CoreID)
	if err != nil {
		fmt.Println(err)
	}
	j.Model = procdata.ModelName
	j.Cores = (procs + 1)
	j.Processors = (procdata.CPU + 1)
	j.CPUSpeed = procdata.Mhz
	fmt.Println(j)
	wg.Done()
}

// CollectMemInfo collects info on system memory
func (j *Jblob) collectMemInfo() {
	memdata, err := mem.VirtualMemory()
	if err != nil {
		fmt.Println(err)
		wg.Done()
	}
	j.TotalMem = memdata.Total
	j.UsedMem = memdata.Used
	j.FreeMem = memdata.Free
	fmt.Println(j)
	wg.Done()
}

// CollectProductInfo collects systems info
func (j *Jblob) collectProductInfo() {
	filePaths := modeSelect()
	fmt.Println(filePaths)

	for k, v := range filePaths {
		fmt.Println("Key:", k)
		fmt.Println("Value:", v)
		data, err := ioutil.ReadFile(v)
		fmt.Println("Data:", data)
		if err != nil {
			fmt.Println("error:", err)
			wg.Done()
		}
		fmt.Println(string(data))

		switch k {
		case "ProductName":
			j.ProductName = string(data)

		case "ProductModel":
			j.ProductModel = string(data)

		case "ProductServiceTag":
			j.ProductServiceTag = string(data)

		default:
			fmt.Println("No Match")
		}

	}
	wg.Done()

}
