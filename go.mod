module gitlab.com/bryanbarton525/go_get_info

go 1.14

require (
	github.com/shirou/gopsutil v2.20.4+incompatible
	golang.org/x/sys v0.0.0-20200501145240-bc7a7d42d5c3 // indirect
	gopkg.in/yaml.v2 v2.2.8
)
