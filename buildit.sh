for GOOS in linux windows; do
    for GOARCH in 386 amd64 mips64; do
        go build -v -o /home/bbarton/go_bins/go_get_info/go-get-info-$GOOS-$GOARCH
    done
done
