package main

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/bryanbarton525/go_get_info/checks"
)

func main() {

	// Define new command line flag
	runArg := flag.NewFlagSet("run", flag.ExitOnError)
	// Define new subcommands
	appArgs := runArg.String("app", "", "Set app=GetSysInfo to execute a single check of just the system stats. \nOptions:\nGetSysInfo\n")
	// Check args slice length
	if len(os.Args) < 2 {
		fmt.Println("Executing full check")
		runAll()
		return
	}
	// Conditional statement to allow for different run subcommands
	switch os.Args[1] {
	case "run":
		runArg.Parse(os.Args[2:])
		if *appArgs == "GetSysInfo" {
			i := checks.GetSysInfo()
			fmt.Println(i)
		} else if *appArgs == "GetClientID" {
			cid := checks.Getcid()
			fmt.Println(cid)
		} else {
			fmt.Println("Executing full check")
			runAll()
		}
	}

}

func runAll() {
	checks.GetSysInfo()
	cid := checks.Getcid()
	fmt.Println(cid)
}
