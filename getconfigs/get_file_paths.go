package getconfigs

import (
	"fmt"
	"io/ioutil"
	"os"

	"gopkg.in/yaml.v2"
)

// Configs is a struct to house the file paths needed in other modules
type Configs struct {
	Mode string
	Prod map[string]string
	Dev  map[string]string
}

var config Configs

// Method to parse mode config yml and write it to the struct var config
func (c *Configs) parseModeConfig() {
	cwd, err := os.Getwd()
	if err != nil {
		fmt.Println(err)
	}
	yamlMode, err := ioutil.ReadFile(cwd + "/modeConfig.yml")
	if err != nil {
		fmt.Println(err)
	}
	err = yaml.Unmarshal(yamlMode, &config)
	if err != nil {
		panic(err)
	}
}

// Method to parse config yml files and write them to struct var config
func (c *Configs) parsePathConfig() {
	cwd, err := os.Getwd()
	if err != nil {
		fmt.Println(err)
	}
	yamlPaths, err := ioutil.ReadFile(cwd + "/pathConfigs.yml")
	if err != nil {
		fmt.Println(err)
	}
	err = yaml.Unmarshal(yamlPaths, &config)
	if err != nil {
		panic(err)
	}

}

// GetConfigs is a function to parse config yamls and return struct
func GetConfigs() *Configs {
	config.parseModeConfig()
	config.parsePathConfig()

	return &config
}
